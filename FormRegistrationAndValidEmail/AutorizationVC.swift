//  AAutorizationVC.swift
//  FormRegistrationAndValidEmail
//  Created by Viktoriia Skvarko


import UIKit

class AutorizationVC: UIViewController {
    
    @IBAction func buttonCancel(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func buttonRegister(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    
    @IBOutlet weak var emailPlaceholder: UITextField!
    
    @IBOutlet weak var passwordPlaceholder: UITextField!
    
    @IBOutlet weak var lableAutorizOrNo: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lableAutorizOrNo.text = ""
        
        // преображение textField:
        
        self.emailPlaceholder.layer.cornerRadius = 6
        self.emailPlaceholder.layer.borderWidth = 2
        self.emailPlaceholder.layer.borderColor = UIColor.darkGray.cgColor
        
        self.passwordPlaceholder.layer.cornerRadius = 6
        self.passwordPlaceholder.layer.borderWidth = 2
        self.passwordPlaceholder.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    @IBAction func singInButton(_ sender: UIButton) {
        
        // валидация e-mail:
        
        func isValidEmail(testStr:String) -> Bool {
            let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            let result = emailTest.evaluate(with: testStr)
            return result
        }
        
        
        let proverka = ""
        if (emailPlaceholder.text != proverka) && (passwordPlaceholder.text != proverka) {
            
            let forProverki = emailPlaceholder.text!
            let validatorEmail = isValidEmail(testStr: forProverki)
            
            if validatorEmail == true {
                self.lableAutorizOrNo.textColor = .green
                lableAutorizOrNo.text = "Вы успешно авторизованы!"
            } else {
                self.lableAutorizOrNo.textColor = .red
                lableAutorizOrNo.text = "Некорректный e-mail!"
            }
        } else {
            self.lableAutorizOrNo.textColor = .red
            lableAutorizOrNo.text = "Не все поля заполнены!"
        }
    }
    
}
