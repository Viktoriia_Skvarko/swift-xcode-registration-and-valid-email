//  VosstanovlenieParolya.swift
//  FormRegistrationAndValidEmail
//  Created by Viktoriia Skvarko



import UIKit

class VosstanovlenieParolya: UIViewController {
    
    @IBOutlet weak var emailVosstanovParolya: UITextField!
    
    @IBOutlet weak var statusVosstanovLable: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailVosstanovParolya.layer.cornerRadius = 6
        self.emailVosstanovParolya.layer.borderWidth = 2
        self.emailVosstanovParolya.layer.borderColor = UIColor.darkGray.cgColor
        
        statusVosstanovLable.text = ""
        
    }
    
    @IBAction func buttonVosstanovlenie(_ sender: Any) {
        
        // валидация e-mail:
        
        func isValidEmail(testStr:String) -> Bool {
            let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            let result = emailTest.evaluate(with: testStr)
            return result
        }
        
        
        let dlyaProverki = emailVosstanovParolya.text!
        let validEmail = isValidEmail(testStr: dlyaProverki)
        
        if validEmail == true {
            self.statusVosstanovLable.textColor = .green
            statusVosstanovLable.text = "Пароль отправлен на Ваш e-mail!"
        } else {
            self.statusVosstanovLable.textColor = .red
            statusVosstanovLable.text = "Некорректный e-mail!"
        }
        
    }
    
}
